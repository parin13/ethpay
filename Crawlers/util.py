import pymysql
import configparser
import requests
import json
import pika
import logging
import os
import datetime
from decimal import Decimal
import exception_str
import web3
import hashlib
import base64
from Crypto.Cipher import AES
from Crypto import Random

# Config Source
conf_file = '/var/www/ethpay/conf/conf.ini'
config = configparser.RawConfigParser()
config.read(conf_file)

# Node URL
URL = config.get('node', 'url')

# Encryption
l1_start = 0
l1_end = 16


class MyLogger():
    """
    Class For Handling Logging
    """

    def __init__(self, directory, category):

        print('In MyLogger')
        self.category = category
        str_date = str(datetime.date.today()).replace('-', '_')
        file_path = os.path.join(directory, str_date + '.txt')

        logging.basicConfig(
            filename=file_path,
            filemode='a',
            format='%(asctime)s,%(msecs)d | %(name)s | %(levelname)s | %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
            level=logging.INFO
        )
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger("pika").setLevel(logging.CRITICAL)
        logging.getLogger("pymysql").setLevel(logging.CRITICAL)
        self.logger = logging.getLogger()
        print('In MyLogger - Completed')

    def msg_logger(self,msg):
        self.logger.info('#'*100)
        self.logger.info(msg)
        self.logger.info('#' * 100)

    def error_logger(self,error):
        self.logger.error('#' * 100)
        self.logger.error(error)
        self.logger.error('#' * 100)

        insert_sql(self, 'error_logs', {
            'category': self.category,
            'file_name': os.path.basename(__file__),
            'error': error,
            'timestamp': datetime.datetime.now()
        })


# MYSQL - Starts
def get_db_connect(logger):
    """
    MySQL Connection
    :return: DB object
    """
    try:
        db = pymysql.connect(
            host = config.get('sql', 'host'),
            user = config.get('sql', 'user'),
            passwd = config.get('sql', 'passwd'),
            db = config.get('sql', 'db')
        )
        return db
    except Exception as e:
        logger.error_logger('Error get_db_connect : %s ' % (str(e)))


def query_from_filter(filters, type='AND'):
    params = ''
    for key, value in filters.items():
        params += "%s = '%s' %s " % (key, value, type)
    return params[:-(len(type)+2)]


def query_from_filter_join(filters, type='AND'):
    params = ''
    for key, value in filters.items():
        params += "%s = %s %s " % (key, value, type)
    return params[:-(len(type)+2)]


def insert_sql(logger, table_name, data):
    """
    Custom method for Insert SQL Query
    :param logger:
    :param table_name:
    :param data:
    :return:
    """
    try:
        db = get_db_connect(logger)
        ret_status = False
        cursor = db.cursor()
        query = 'insert into %s(%s) values(%s)' % (table_name, ','.join([key for key in data]),','.join(['%s' for _ in data]))
        values = tuple([value for key,value in data.items()])
        cursor.execute(query,(values))
        db.commit()
        ret_status = True
        logger.msg_logger('>>>>>>>> MYSQL Insert Success : %s || %s' % (query, str(data)))
    except Exception as e:
        logger.error_logger('insert_sql : %s || %s || %s'%(str(e),query,str(data)))
    finally:
        if db : db.close()
        return ret_status


def find_sql(logger, table_name, filters, columns=''):
    """
    Custom method for Select SQL Query
    :param logger:
    :param table_name:
    :param filters:
    :param columns:
    :return:
    """
    try:
        data = None
        db = get_db_connect(logger)
        cursor = db.cursor(pymysql.cursors.DictCursor)

        if columns:
            columns = ','.join(columns)
        else:
            columns = '*'

        params = query_from_filter(filters)
        query = 'SELECT %s FROM %s WHERE %s'%(columns,table_name,params)
        cursor.execute(query)
        data = cursor.fetchall()
        logger.msg_logger('>>>>>>>> MYSQL Find Success : %s' % (query))
    except Exception as e:
        logger.error_logger('find_sql : %s || %s'%(str(e),query))
    finally:
        if db: db.close()
        return data


def find_sql_join(logger, table_names, filters, on_conditions, type='INNER',columns=''):
    """
    Custom method for Insert SQL Query with Joins
    :param logger:
    :param table_names:
    :param filters:
    :param on_conditions:
    :param type:
    :param columns:
    :return:
    """
    try:
        data = None
        db = get_db_connect(logger)
        cursor = db.cursor(pymysql.cursors.DictCursor)

        if columns:
            columns = ','.join(columns)
        else:
            columns = '*'

        on = query_from_filter_join(on_conditions)
        params = query_from_filter(filters)

        join = ''
        for table in table_names:
            join += '%s %s %s JOIN '%(table,table,type)
        join = join[:-(len(type)+7)] # Removing Join String


        query = 'SELECT %s FROM %s ON %s WHERE %s'%(columns,join,on,params)
        cursor.execute(query)
        data = cursor.fetchall()
        logger.msg_logger('>>>>>>>> MYSQL Find Success : %s' % (query))
    except Exception as e:
        logger.error_logger('find_sql_join : %s || %s'%(str(e),query))
    finally:
        if db: db.close()
        return data


def update_sql(logger, table_name, filters, updated_values):
    """
    Custom method for Update SQL Query
    :param logger:
    :param table_name:
    :param filters:
    :param updated_values:
    :return:
    """
    try:
        ret_status = False
        db = get_db_connect(logger)
        cursor = db.cursor()
        update_params = query_from_filter(updated_values,type=',')
        filter_params = query_from_filter(filters)

        # Check if present
        if find_sql(logger, table_name=table_name, filters=filters) :
            query = 'UPDATE %s SET %s WHERE %s' % (table_name, update_params, filter_params)
            cursor.execute(query)
            db.commit()
            logger.msg_logger('>>>>>>>> MYSQL update Success : %s' % (query))
            ret_status = True

    except Exception as e:
        print(e)
        logger.error_logger('update_sql : %s || %s'%(str(e),query))
    finally:
        if db : db.close()
        return ret_status


def increment_sql(logger, table_name, filters, column):
    """
    Custom method for Update(+1) SQL Query
    :param logger:
    :param table_name:
    :param filters:
    :param column:
    :return:
    """
    try:
        ret_status = False
        db = get_db_connect(logger)
        cursor = db.cursor()
        params = query_from_filter(filters)
        if find_sql(logger, table_name=table_name, filters=filters):
            query = 'UPDATE %s SET %s = %s + 1 WHERE %s'%(table_name,column,column,params)
            cursor.execute(query)
            db.commit()
            ret_status = True
            logger.msg_logger('>>>>>>>> MYSQL Increment Success : %s' % (query))
    except Exception as e:
        print(e)
        logger.error_logger('increment_sql : %s || %s'%(str(e),query))
    finally:
        if db : db.close()
        return ret_status

# MYSQL - Ends

def rpc_request(logger, method, params, url=''):
    """
    Custom RPC Method
    """
    try :
        if not url:
            url = config.get('node', 'url')

        headers = {'Content-type': 'application/json'}
        payload = {
            "jsonrpc": "2.0",
            "id": 1,
            "method":method,
            "params": params
        }
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        return json.loads(response.text)
    except Exception as e:
        logger.error_logger('rpc_request : %s' % (e))


def send_notification(logger, notif_url,notif_params, queue):
    """
    Sending Notification to Rabbit MQ
    :param logger:
    :param notif_url:
    :param notif_params:
    :param queue:
    :return:
    """
    try:
        if not notif_url:
            logger.error_logger('>>>>>>>>>> Notification URL is empty : %s'%(notif_params))
            # TODO - Raise Exception
            return False

        notif_params['notification_url'] = notif_url
        logger.msg_logger('>>>>>>>>>> Sending Notification : %s || %s' % (notif_url, notif_params))
        # Rabbit MQ
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        channel.queue_declare(queue=queue, durable=True)
        channel.basic_publish(exchange='', routing_key=queue, body=json.dumps(notif_params),properties=pika.BasicProperties(delivery_mode=2))
        connection.close()
        logger.msg_logger('>>>>>>>>>> Hook Queue : %s ' % (queue))
    except Exception as e:
        logger.error_logger('send_notification : %s'%(e))

def blockchain_connection(obj_logger, url):
    """
    Connect To Blockchain Node
    """
    provider = web3.Web3().HTTPProvider(url)
    return web3.Web3(providers=[provider])


def sign_transaction(obj_logger, from_address, to_address, value, private_key):
    """
    For Signing Transaction
    """
    # Future Use - Only Specific characters are allowed - Hexadecimal
    bid_id = '1'

    con = blockchain_connection(obj_logger, URL)

    # Sign
    transaction = {
        'to': web3.Web3().toChecksumAddress(to_address),
        'value': web3.Web3().toHex(web3.Web3().toWei(value, unit='ether')),
        'gas': int(config.get('eth', 'gas_limit')),
        'gasPrice': con.eth.gasPrice,
        'nonce': con.eth.getTransactionCount(web3.Web3().toChecksumAddress(from_address)),
        'chainId' : int(config.get('node', 'chain_id')),
        'data' : bid_id
    }

    signed = web3.Account.signTransaction(transaction, private_key)
    return signed.rawTransaction.hex()



def forward_etherum(obj_logger, token, from_address, value):
    """
    To Transfer Ethereum
    """
    try:

        # Getting data of from address
        try:
            to_address = config.get('forward', 'address')
        except:
            raise Exception('Address Not Found in Config File')

        # To Handle forwarding from block crawler
        if from_address == to_address:
            return

        # Decrypt Private Key
        enc_private_key = find_sql(logger=obj_logger, table_name='address_master', filters={'address': from_address})[0]['private_key']
        if not enc_private_key:
            raise Exception(exception_str.UserExceptionStr.private_key_not_found)
        log = 'block'
        private_key = AESCipher(token, log).decrypt(enc_private_key)

        # Create Transaction Sign
        sign = sign_transaction(obj_logger=obj_logger, from_address=from_address, to_address=to_address, value=value, private_key=private_key)

        # Create Raw Transaction
        method = 'eth_sendRawTransaction'
        params = [sign]
        response = rpc_request(obj_logger, method, params)

        if not response:
            raise Exception(response.get('error', {}).get('message', exception_str.UserExceptionStr.node_down))

        if response.get('error',''):
            raise Exception(response.get('error',{}).get('message',exception_str.UserExceptionStr.unknown_exception))

        tx_hash = response.get('result','')

        if not tx_hash:
            raise Exception(exception_str.UserExceptionStr.some_error_occurred)

        obj_logger.msg_logger('Forward transaction is done from block: %s'%(tx_hash))

    except Exception as e:
        obj_logger.error_logger('forward_etherum : %s' % (e))


def get_config(log):
    return config.get(log, 'logs'),config.get(log, 'category')


class AESCipher():
    """
    AES Cipher Encryption
    Source : https://stackoverflow.com/questions/12524994/encrypt-decrypt-using-pycrypto-aes-256
    """

    def __init__(self, key, log):
        try:
            # Logs
            self.logs_directory, self.category = get_config(log)
            self.obj_logger = MyLogger(self.logs_directory, self.category)

            self.bs = 32
            self.key = self.generate_key(key)
            self.key = hashlib.sha256(self.key.encode()).digest()

        except Exception as e:
            print(e)
            self.obj_logger.error_logger("Error AESCipher __init__ : " + str(e))


    def encrypt(self, raw):
        try:
            raw = self._pad(raw)
            raw = raw.encode('utf-8')
            iv = Random.new().read(AES.block_size)
            cipher = AES.new(self.key, AES.MODE_CBC, iv)
            return base64.b64encode(iv + cipher.encrypt(raw))
        except Exception as e:
            print(e)
            self.obj_logger.error_logger("Error AESCipher encrypt : " + str(e))

    def decrypt(self, enc):
        try:
            enc = base64.b64decode(enc)
            iv = enc[:AES.block_size]
            cipher = AES.new(self.key, AES.MODE_CBC, iv)
            return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
        except Exception as e:
            self.obj_logger.error_logger("Error AESCipher decrypt : " + str(e))

    def _pad(self, s):
        try:
            return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)
        except Exception as e:
            print(e)
            self.obj_logger.error_logger("Error AESCipher _pad : " + str(e))

    def _unpad(self, s):
        try:
            return s[:-ord(s[len(s)-1:])]
        except Exception as e:
            print(e)
            self.obj_logger.error_logger("Error AESCipher _unpad : " + str(e))

    def generate_key(self, key):
        """
        This method is used for creating key for aes cipher
        :param input: token number
        :return: sha256 of the input
        """
        try:
            token_key = hashlib.sha256(key.encode()).hexdigest()
            l1_token_key = token_key[l1_start:l1_end]
            return l1_token_key
        except Exception as e:
            print(e)
            self.obj_logger.error_logger("Error generate_key : " + str(e))
