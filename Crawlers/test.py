from util import insert_sql, rpc_request, send_notification, find_sql_join, MyLogger, config


logs_directory = config.get('mempool', 'logs')
category = config.get('mempool', 'category')
obj_logger = MyLogger(logs_directory, category)
obj_logger.msg_logger('Getting Mempool Data')


tx_id = '0x8fc10f925f2755a4f451444442e3a9e8870060ffebc2556c9fafe8a16275eee1'

tx_data = rpc_request(obj_logger, 'eth_getTransactionByHash', [tx_id])
tx = tx_data['result']
print(tx.get('input','')[2:] if tx.get('input','') else '')