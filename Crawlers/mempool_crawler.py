import os,sys
import datetime
import redis
import web3
from apscheduler.schedulers.blocking import BlockingScheduler
from util import insert_sql, rpc_request, send_notification, find_sql_join, MyLogger, config

# Redis Connection
pool = redis.ConnectionPool(
    host = config.get('redis', 'host'),
    port = int(config.get('redis', 'port')),
    db = int(config.get('redis', 'db'))
)
redis_conn = redis.Redis(connection_pool=pool)

# Blockchain Node
logs_directory = config.get('mempool', 'logs')
category = config.get('mempool', 'category')
hook_queue = config.get('hook_main', 'queue')

def mempool_crawler():
    """
    Mempool Process
    :return:
    """

    obj_logger = MyLogger(logs_directory,category)
    obj_logger.msg_logger('Getting Mempool Data')

    # Get Mempool Data
    mempool_transaction_data = rpc_request(obj_logger, 'eth_getBlockByNumber', ['pending', True])

    if not mempool_transaction_data.get('result',{}).get('transactions',[]):
        raise Exception('Blockchain Node is Down')

    mempool_transaction_data = mempool_transaction_data.get('result', {}).get('transactions', [])

    obj_logger.msg_logger('Crawling Mempool Starts')

    for tx in mempool_transaction_data:

        tx_hash = tx['hash']
        to_address = tx['to'].encode('utf-8') if tx['to'] else ''

        # Redis Check
        if (not redis_conn.sismember('eth_zct_set',tx_hash)) and (redis_conn.sismember('eth_aw_set',to_address)):
            obj_logger.msg_logger('>>>>>>>> Transaction Found in Mempool : %s'%(tx_hash))

            from_address = tx['from']
            value = web3.Web3().fromWei(int(tx['value'],16), unit='ether')
            confirmations = 0
            flag = 'incoming'
            sys_timestamp = datetime.datetime.now()
            bid_id = tx.get('input','')[2:] if tx.get('input','') else '' # Removing 0x -> [2:]
            block_number = -1

            # Insert in DB
            result = insert_sql(
                logger=obj_logger,
                table_name= 'transaction_master',
                data={
                'from_address': from_address,
                'to_address': to_address,
                'tx_hash': tx_hash,
                'bid_id': bid_id,
                'confirmations': confirmations,
                'block_number': block_number,
                'value': value,
                'flag': flag,
                'sys_timestamp': sys_timestamp,
                }
            )

            if result:
                notif_url = find_sql_join(logger=obj_logger,
                    table_names=['user_master', 'address_master'],
                    filters={'address_master.address': str(to_address.decode('utf-8'))},
                    on_conditions={'user_master.user_name': 'address_master.user_name'},
                    columns=['user_master.notification_url']
                )

                if not notif_url:
                    obj_logger.error_logger('Notification URL not found for address  : %s' % (to_address))
                else:
                    notif_url = notif_url[0]['notification_url']
                    notif_params = {
                        'from_address': from_address,
                        'to_address': to_address,
                        'tx_hash': tx_hash,
                        'bid_id': bid_id,
                        'confirmations': confirmations,
                        'block_number': block_number,
                        'value': value,
                        'flag': flag
                    }
                    send_notification(obj_logger,notif_url,notif_params,queue=hook_queue)
                    obj_logger.msg_logger('>>>>>>>> Adding to zct_set : %s' % (tx_hash))

            # Zero Confirmation Redis Set
            redis_conn.sadd('eth_zct_set', tx_hash.encode('utf-8'))

    obj_logger.msg_logger('Crawling Mempool Ends')

def main():
    """
    Scheduling
    :return:
    """
    try:
        sched = BlockingScheduler(timezone='Asia/Kolkata')
        sched.add_job(mempool_crawler, 'interval', id='mempool_crawler', seconds=3)
        sched.start()
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger = MyLogger(logs_directory,category)
        obj_logger.error_logger('Main : %s'%(e))


if __name__ == '__main__':
    main()
