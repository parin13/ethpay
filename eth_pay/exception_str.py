class UserExceptionStr():
    """
    Raw Data for End User Information/Exception
    """
    some_error_occurred = 'Some Error Occurred'
    bad_request = 'Bad Request!'
    specify_required_fields = 'Please specify all required fields!'
    input_params_wrong = 'Input Parameters are wrong.'
    insufficient_funds_ether = 'Insufficient funds in Ether'
    insufficient_tokens = 'Insufficient tokens in account'
    invalid_user = 'Invalid User'
    not_user_address = 'This Address doesnot correspond to the User.'
    duplicate_transaction = 'Same transaction already happened'
    error_request_data = 'Error while getting Request Data'
    unknown_exception = 'Unknown Exception'
    private_key_not_found = 'Private Key Not Found!'