import os, sys
import math
import web3
import models
import exception_str
import custom_exception
import common_util
import datetime
from decimal import Decimal

# Gas Limit
gas_limit = int(common_util.config.get('eth', 'gas_limit'))
chainId = int(common_util.config.get('node', 'chain_id'))

# Log
log = 'end_points'
logs_directory, category = common_util.get_config(log)

# Common Methods for eth and erc
obj_common = common_util.CommonUtil(log=log)

def get_fee():
    """
    To Get Token Transfer Fee
    """
    try:
        obj_logger = common_util.MyLogger(logs_directory, category)
        # Create connection
        con = obj_common.blockchain_connection(common_util.url)
        # RPC
        gas_price = con.eth.gasPrice
        fee = gas_limit*gas_price
        fee = web3.Web3.fromWei(fee, unit='ether')
        return fee

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger.error_logger('get_fee : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def get_ether_balance(user_name, user_address):
    """
    To Get user token Balance
    """
    try:
        # Check if the address correspond to the user
        obj_logger = common_util.MyLogger(logs_directory, category)
        if not models.find_sql(logger=obj_logger, table_name='address_master', filters={'user_name': user_name, 'address': user_address.lower()}):
            raise custom_exception.UserException(exception_str.UserExceptionStr.not_user_address)

        balance = obj_common.get_ether_balance(web3.Web3().toChecksumAddress(user_address))
        balance = web3.Web3.fromWei(balance, unit='ether')
        return balance

    except custom_exception.UserException:
        raise
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger.error_logger('get_ether_balance : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def generate_address(user_name, token):
    try:
        # Logs
        obj_logger = common_util.MyLogger(logs_directory, category)

        # Generate Crypto Secure Number
        crypto_secure = ''.join([str(int(math.pow(number, 2))) for number in os.urandom(20)])

        # Generate Private Key
        private_key = web3.Web3().sha3(text=crypto_secure).hex()

        # Genrate public Address
        address = web3.Account.privateKeyToAccount(private_key).address.lower()

        # Encrypt PK
        enc_pk = common_util.AESCipher(token,log).encrypt(private_key)

        # Insert in DB
        models.insert_sql(
            logger=obj_logger,
            table_name='address_master',
            data={
                'user_name': user_name,
                'address': address,
                'private_key': enc_pk,
                'timestamp' : datetime.datetime.now()
            })

        # Check if encryption algorithm pass
        enc_pk = models.find_sql(obj_logger,'address_master',{'user_name':user_name,'address':address})[0]['private_key']
        dec_pk = common_util.AESCipher(token,log).decrypt(enc_pk)
        if private_key != dec_pk:
            obj_logger.error_logger('Encryption Error')
            raise custom_exception.UserException(exception_str.UserExceptionStr.some_error_occurred)

        # Add in Redis for Crawlers    
        obj_common.get_redis_connection().sadd('eth_aw_set', address.lower().encode('utf-8'))
        return address

    except custom_exception.UserException:
        raise
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger.error_logger('generate_address : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def sign_transaction(from_address, to_address, value, private_key):
    """
    For Signing Transaction
    """
    try:
        # Logger
        obj_logger = common_util.MyLogger(logs_directory, category)

        # Future Use - Only Specific characters are allowed - Hexadecimal
        bid_id = '1'

        con = obj_common.blockchain_connection(common_util.url)

        # Sign
        transaction = {
            'to': web3.Web3().toChecksumAddress(to_address),
            'value': web3.Web3().toHex(web3.Web3().toWei(value, unit='ether')),
            'gas': gas_limit,
            'gasPrice': con.eth.gasPrice,
            'nonce': con.eth.getTransactionCount(web3.Web3().toChecksumAddress(from_address)),
            'chainId' : chainId,
            'data' : bid_id
        }

        signed = web3.Account.signTransaction(transaction, private_key)
        return signed.rawTransaction.hex()

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger.error_logger('sign_transaction : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def forward_batch(user_name, token, to_address, value):
    """
    To Transfer Ethereum
    """
    try:

        # logger
        obj_logger = common_util.MyLogger(logs_directory, category)

        # Getting data of from address
        try:
            from_address = common_util.config.get('forward', 'address')
            enc_private_key_path = common_util.config.get('forward', 'private_key')
            enc_private_key = None
            with open(enc_private_key_path, 'r') as obj_file:
                enc_private_key = obj_file.readlines()[0]  # private key should be in the first line
        except:
            raise custom_exception.UserException(exception_str.UserExceptionStr.private_key_not_found)

        if not enc_private_key:
            raise custom_exception.UserException(exception_str.UserExceptionStr.private_key_not_found)

        # value
        value = Decimal(value)

        # Get User Ether Balance in Wei
        balance = Decimal(get_ether_balance(user_name, from_address))

        # Transaction Fee in
        tx_fee = Decimal(get_fee())

        # Check if the transaction fee > eth_balance
        if (tx_fee > balance) or (value + tx_fee > balance):
            raise custom_exception.UserException(exception_str.UserExceptionStr.insufficient_funds_ether)

        # Decrypt Private Key
        private_key = common_util.AESCipher(token, log).decrypt(enc_private_key)

        # Create Transaction Sign
        sign = sign_transaction(from_address=from_address, to_address=to_address, value=value, private_key=private_key)

        # Create Raw Transaction
        method = 'eth_sendRawTransaction'
        params = [sign]
        response = obj_common.rpc_request(common_util.url, method, params)

        if response.get('error',''):
            raise custom_exception.UserException(response.get('error',{}).get('message',exception_str.UserExceptionStr.unknown_exception))

        tx_hash = response.get('result','')

        if not tx_hash:
            raise custom_exception.UserException(exception_str.UserExceptionStr.some_error_occurred)

        return tx_hash

    except custom_exception.UserException:
        raise
    except web3.exceptions.ValidationError as e:
        obj_logger.error_logger('forward_etherum : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.input_params_wrong)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger.error_logger('forward_batch : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)



def forward_etherum(user_name, token, from_address, to_address, value):
    """
    To Transfer Ethereum
    """
    try:
        # value
        value = Decimal(value)

        # Get User Ether Balance in Wei
        balance = Decimal(get_ether_balance(user_name, from_address))

        # Transaction Fee in
        tx_fee = Decimal(get_fee())

        # Check if the transaction fee > eth_balance
        if (tx_fee > balance) or (value + tx_fee > balance):
            raise custom_exception.UserException(exception_str.UserExceptionStr.insufficient_funds_ether)

        # Decrypt Private Key
        obj_logger = common_util.MyLogger(logs_directory, category)
        enc_private_key = models.find_sql(logger=obj_logger, table_name='address_master', filters={'user_name': user_name, 'address':from_address})[0]['private_key']
        if not enc_private_key:
            raise custom_exception.UserException(exception_str.UserExceptionStr.not_user_address)
        private_key = common_util.AESCipher(token, log).decrypt(enc_private_key)

        # Create Transaction Sign
        sign = sign_transaction(from_address=from_address, to_address=to_address, value=value, private_key=private_key)

        # Create Raw Transaction
        method = 'eth_sendRawTransaction'
        params = [sign]
        response = obj_common.rpc_request(common_util.url, method, params)

        if response.get('error',''):
            raise custom_exception.UserException(response.get('error',{}).get('message',exception_str.UserExceptionStr.unknown_exception))

        tx_hash = response.get('result','')

        if not tx_hash:
            raise custom_exception.UserException(exception_str.UserExceptionStr.some_error_occurred)

        return tx_hash

    except custom_exception.UserException:
        raise
    except web3.exceptions.ValidationError as e:
        obj_logger = common_util.MyLogger(logs_directory, category)
        obj_logger.error_logger('Error forward_etherum : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.input_params_wrong)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print(error_stack)
        obj_logger = common_util.MyLogger(logs_directory, category)
        obj_logger.error_logger('forward_eth : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)