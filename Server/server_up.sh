#!/bin/bash
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "******************************** Server Up Script......... ********************************"
echo "*******************************************************************************************"
echo "******************************** Starting mysql-server......... ***************************"
sudo service mysql start
echo "******************************** Starting redis-server......... ***************************"
sudo service redis-server start
echo "******************************** Starting rabbitmq-server......... ************************"
sudo service rabbitmq-server start
echo "******************************** Starting apache2 server......... *************************"
sudo service apache2 start
echo "To run Crawlers do following manually:"
echo "crontab -e"
echo "copy these 2 lines to crontab"
echo "*/1 * * * * python3.6 /var/www/ethpay/Crawlers/mempool_crawler.py"
echo "*/1 * * * * python3.6 /var/www/ethpay/Crawlers/block_crawler.py"
echo "write into default crontab file and exit"
echo "after that run the following command"
echo "nohup python3.6 /var/www/ethpay/Crawlers/hook_consumer.py >/dev/null 2>&1 & #"
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "*******************************************************************************************"