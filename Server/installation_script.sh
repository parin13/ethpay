#!/bin/bash
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "******************************** Synchronizing the package index files.... ****************"
echo "********************************        OR                  *******************************"
echo "******************************** Doing apt update..... ************************************"
echo "*******************************************************************************************"
sudo apt update
sudo ./install_sql_server_script.sh
sudo ./install_python_pip_and_packages.sh
sudo python3.6 ./create_db_script.py
sudo ./install_redis.sh
sudo ./install_rabbitmq.sh
sudo ./install_apache.sh
sudo ./create_log_folders.sh
# End